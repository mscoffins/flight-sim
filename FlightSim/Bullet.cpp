#include "Bullet.h"

Bullet::Bullet(ThreeDModel* b){
	dir = glm::vec3(0,0,1);
	pos = glm::vec3(0,0,0);
	orientation = glm::mat4(1.0);
	bullet = b;
	speed = 0;
	ttl = 2;
}
void Bullet::update(float deltaT){
	pos += dir * speed * deltaT;
	ttl-=deltaT;
	speed-= 100 * deltaT;
}
void Bullet::draw(Shader* s){
	bullet->drawElementsUsingVBO(s);
}