#pragma once
#include <math.h>
#include "glm\glm.hpp"
using namespace glm;


class Collisions{
public:
	 static float squared(float v) { return v * v; }
	 static bool sphereBoxColl(vec3 C1, vec3 C2, vec3 S, float R);
	 static float onLeftTri(vec3 a, vec3 b, vec3 c, vec3 d);
	 static bool onSameSide(float * vals, int size);
	 static vec3 getTriNormal(vec3 p1, vec3 p2, vec3 p3);
	 static int shortestDist(float* dets, int size);
	 static bool intersectAtAxis(vec3* aVerts, vec3* bVerts, vec3 axis);
	 static bool intersectAtAxisTri(vec3* aVerts, vec3* bVerts, vec3 axis);
	 static bool OBBOBBCollision(vec3* aVerts, vec3* bVerts, vec3* aNorms, vec3* bNorms);
	 static bool OBBTriCollision(vec3* bVerts, vec3* tVerts, vec3* bNorms, vec3 tNorm);
	 static vec3* calcBoxNormals(vec3* boxVerts);
};