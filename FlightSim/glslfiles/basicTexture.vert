#version 150

uniform mat4 ModelViewMatrix;

in  vec3 in_Position;  // Position coming in

in vec3 in_TexCoord;   // texture coordinate coming in to the vertex
out vec2 ex_TexCoord;  // texture coordinate going out to the fragment shader

void main(void)
{
	gl_Position = ModelViewMatrix * vec4(in_Position, 1.0);
	
	ex_TexCoord = vec2(in_TexCoord.x,in_TexCoord.y);
}