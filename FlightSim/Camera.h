#pragma once;
#include "Plane.h"
#include "glm\glm.hpp"
#include "glm\gtc\quaternion.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtc\type_ptr.hpp"
#include "glm\gtc\matrix_inverse.hpp"
#include "glm\gtx\rotate_vector.hpp"

enum CamType{FIRST_PERSON, THIRD_PERSON, STATIC, WING, WHEEL, TAIL};

class Camera{
private:
	glm::vec3 pos;
	glm::vec3 target;
	glm::vec3 up;
	Plane* plane;
	
public:
	Camera(glm::vec3 p = glm::vec3(0,0,0),Plane* plane = new Plane(), CamType type = STATIC);
	glm::vec3 getPos(){return pos;}
	glm::vec3 gettarget(){return target;}
	glm::vec3 getUp(){return up;}
	void setPos(glm::vec3 p){pos = p;}
	void setUp(glm::vec3 u){up = u;}
	void update(float deltaT);
	
	CamType type;
};