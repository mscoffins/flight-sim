#include "Camera.h"

Camera::Camera(glm::vec3 p,Plane* plane, CamType type){
	target = plane->getCentre() + plane->getForward()*100.0f;
	
	switch(type){
		case FIRST_PERSON:
			pos = plane->getCentre() + plane->getUp() * 25.0f - plane->getForward() * 2.0f;
			up = plane->getUp();
			break;
		case THIRD_PERSON:
			pos = plane->getCentre() - plane->getForward() * 250.0f + plane->getUp() * 75.0f;
			up = plane->getUp();
			break;
		case STATIC:
			pos = p;
			target = plane->getCentre();
			up = glm::vec3(0,1,0);
			break;
		case WING:
			pos = plane->getCentre() + plane->getUp() * 25.0f - plane->getForward() * 2.0f + plane->getRight() * 20.0f;
			up = plane->getUp();
			break;
		case WHEEL:
			pos = plane->getCentre() - plane->getUp() * 25.0f - plane->getForward() * 5.0f;
			up = plane->getUp();
			break;
		case TAIL:
			pos = plane->getCentre() + plane->getUp() * 25.0f - plane->getForward() * 30.0f + plane->getRight() * 10.0f;
			up = plane->getUp();
			break;

	}
	this->plane = plane;
	this->type = type;
}

void Camera::update(float deltaT){
	target = plane->getCentre() + plane->getForward()*200.0f;
	vec3 prevPos, newPos, dist, prevUp, newUp, upDist;
	switch(type){
		case FIRST_PERSON:
			pos = plane->getCentre() + plane->getUp() * 15.0f - plane->getForward() * 2.0f;
			up = plane->getUp();
			break;
		case THIRD_PERSON:
			prevPos = pos;
			newPos = plane->getCentre() - plane->getForward() * 250.0f + plane->getUp() * 50.0f;
			dist = newPos - prevPos;
			//if((dist.x * dist.x + dist.y * dist.y + dist.z + dist.z)>50)
			pos += dist*5.0f * deltaT;//plane->getCentre() - plane->getForward() * 250.0f + plane->getUp() * 75.0f;
			prevUp = up;
			newUp = plane->getUp();
			upDist = newUp - prevUp;
			//if((upDist.x * upDist.x + upDist.y * upDist.y + upDist.z + upDist.z)>5)
			up += upDist * 5.0f * deltaT;//plane->getUp();
			break;
		case STATIC:
			target = plane->getCentre();
			break;
		case WING:
			target = plane->getCentre() + plane->getForward()*300.0f + plane->getRight() * 75.0f;
			pos = plane->getCentre() + plane->getUp() * 5.0f - plane->getForward() * 50.0f + plane->getRight() * 40.0f;
			up = plane->getUp();
			break;
		case WHEEL:
			pos = plane->getCentre() - plane->getUp() * 25.0f - plane->getForward() * 50.0f;
			up = plane->getUp();
			break;
		case TAIL:
			pos = plane->getCentre() + plane->getUp() * 15.0f - plane->getForward() * 130.0f + plane->getRight() * 25.0f;
			up = plane->getUp();
			break;

	}
}