#include "Collisions.h"

bool Collisions::sphereBoxColl(vec3 C1, vec3 C2, vec3 S, float R)
{
    float dist_squared = R * R;
    /* assume C1 and C2 are element-wise sorted, if not, do that now */
    if (S.x < C1.x) dist_squared -= squared(S.x - C1.x);
    else if (S.x > C2.x) dist_squared -= squared(S.x - C2.x);
    if (S.y < C1.y) dist_squared -= squared(S.y - C1.y);
    else if (S.y > C2.y) dist_squared -= squared(S.y - C2.y);
    if (S.z < C1.z) dist_squared -= squared(S.z - C1.z);
    else if (S.z > C2.z) dist_squared -= squared(S.z - C2.z);
    return dist_squared > 0;
}

float Collisions::onLeftTri(vec3 a, vec3 b, vec3 c, vec3 d){
	mat4 vertMat =  mat4(vec4(a,1.0),vec4(b,1.0),vec4(c,1.0),vec4(d,1.0));
	return determinant(vertMat);
}

bool Collisions::onSameSide(float * vals, int size){
	bool pos;
	if(vals[0] > 0)
		pos = true;
	else
		pos = false;

	for(int i = 1; i < size; i++){
		bool temp;
		if(vals[i] > 0)
			temp = true;
		else
			temp = false;
		if(temp != pos)
			return false;
	}
	return true;
}

vec3 Collisions::getTriNormal(vec3 p1, vec3 p2, vec3 p3){
	return normalize(cross(p2 - p1, p3 - p1));
}

int Collisions::shortestDist(float* dets, int size){
	int currentMinLoc = 0;
	float currentMin = dets[0];
	for(int i = 1; i < size; i++){
		if(dets[i] < currentMin){
			currentMinLoc = i;
			currentMin = dets[i];
		}
	}
	return currentMinLoc;
}

bool Collisions::intersectAtAxis(vec3* aVerts, vec3* bVerts, vec3 axis){
	// Handles the cross product = {0,0,0} case
    if( axis == vec3(0,0,0)) 
        return true;

	float aMin = FLT_MAX;
    float aMax = FLT_MIN;
    float bMin = FLT_MAX;
    float bMax = FLT_MIN;
	
    // Define two intervals, a and b. Calculate their min and max values
    for( int i = 0; i < 8; i++ ) {
        float aDist = dot( aVerts[i], axis );
        aMin = ( aDist < aMin ) ? aDist : aMin;
        aMax = ( aDist > aMax ) ? aDist : aMax;
        float bDist = dot( bVerts[i], axis );
        bMin = ( bDist < bMin ) ? bDist : bMin;
        bMax = ( bDist > bMax ) ? bDist : bMax;
    }
    // One-dimensional intersection test between a and b
    float longSpan = max( aMax, bMax ) - min( aMin, bMin );
    float sumSpan = aMax - aMin + bMax - bMin;
    return longSpan < sumSpan; // Change this to <= if you want the case were they are touching 
}

vec3* Collisions::calcBoxNormals(vec3* boxVerts){
	vec3 normals[3];
	normals[0] = getTriNormal(boxVerts[0], boxVerts[1], boxVerts[2]);
	normals[1] = getTriNormal(boxVerts[2], boxVerts[4], boxVerts[5]);
	normals[2] = getTriNormal(boxVerts[1], boxVerts[4], boxVerts[6]);

	return normals;

}

bool Collisions::OBBOBBCollision(vec3* aVerts, vec3* bVerts, vec3* aNorms, vec3* bNorms){
	vec3 Axis[15];
	Axis[0] = aNorms[0];
	Axis[1] = aNorms[1];
	Axis[2] = aNorms[2];
	Axis[3] = bNorms[0];
	Axis[4] = bNorms[1];
	Axis[5] = bNorms[2];
	Axis[6] = cross(aNorms[0],bNorms[0]);
	Axis[7] = cross(aNorms[0],bNorms[1]);
	Axis[8] = cross(aNorms[0],bNorms[2]);
	Axis[9] = cross(aNorms[1],bNorms[0]);
	Axis[10] = cross(aNorms[1],bNorms[1]);
	Axis[11] = cross(aNorms[1],bNorms[2]);
	Axis[12] = cross(aNorms[2],bNorms[0]);
	Axis[13] = cross(aNorms[2],bNorms[1]);
	Axis[14] = cross(aNorms[2],bNorms[2]);

	for(int i = 0; i < 15; i++){
		bool intersect = intersectAtAxis(aVerts,bVerts, Axis[i]);
		if(!intersect)
			return false;
	}
	return true;


}

bool Collisions::OBBTriCollision(vec3* bVerts, vec3* tVerts, vec3* bNorms, vec3 tNorm){
	vec3 Axis[13];
	vec3 triEdges[3];
	triEdges[0] = normalize(tVerts[1] - tVerts[0]);
	triEdges[1] = normalize(tVerts[2] - tVerts[0]);
	triEdges[2] = normalize(tVerts[2] - tVerts[1]);

	Axis[0] = bNorms[0];
	Axis[1] = bNorms[1];
	Axis[2] = bNorms[2];
	Axis[3] = tNorm;
	Axis[4] = cross(bNorms[0],triEdges[0]);
	Axis[5] = cross(bNorms[0],triEdges[1]);
	Axis[6] = cross(bNorms[0],triEdges[2]);
	Axis[7] = cross(bNorms[1],triEdges[0]);
	Axis[8] = cross(bNorms[1],triEdges[1]);
	Axis[9] = cross(bNorms[1],triEdges[2]);
	Axis[10] = cross(bNorms[2],triEdges[0]);
	Axis[11] = cross(bNorms[2],triEdges[1]);
	Axis[12] = cross(bNorms[2],triEdges[2]);

	for(int i = 0; i < 13; i++){
		bool intersect = intersectAtAxisTri(bVerts,tVerts, Axis[i]);
		if(!intersect)
			return false;
	}
	return true;


}

bool Collisions::intersectAtAxisTri(vec3* bVerts, vec3* tVerts, vec3 axis){
	// Handles the cross product = {0,0,0} case
    if( axis == vec3(0,0,0)) 
        return true;

	float bMin = FLT_MAX;
    float bMax = FLT_MIN;
    float tMin = FLT_MAX;
    float tMax = FLT_MIN;
	
    // Define two intervals, a and b. Calculate their min and max values
    for( int i = 0; i < 8; i++ ) {
        float bDist = dot( bVerts[i], axis );
        bMin = ( bDist < bMin ) ? bDist : bMin;
        bMax = ( bDist > bMax ) ? bDist : bMax;
	}
	for(int i = 0; i < 3; i++){
        float tDist = dot( tVerts[i], axis );
        tMin = ( tDist < tMin ) ? tDist : tMin;
        tMax = ( tDist > tMax ) ? tDist : tMax;
	}
    // One-dimensional intersection test between a and b
    float longSpan = max( bMax, tMax ) - min( bMin, tMin );
    float sumSpan = bMax - bMin + tMax - tMin;
    return longSpan < sumSpan; // Change this to <= if you want the case were they are touching 
}