//OPENGL 3.2 DEMO FOR RENDERING OBJECTS LOADED FROM OBJ FILES

//includes areas for keyboard control, mouse control, resizing the window
//and draws a spinning rectangle
#include <time.h>
#include <windows.h>		// Header File For Windows
#include "gl/glew.h"
#include "gl/wglew.h"
#pragma comment(lib, "glew32.lib")
#include "Images\imageloader.h"
#include "console.h"
#include "shaders/Shader.h"   // include shader header file, this is not part of OpenGL

#include "Plane.h"
#include "Camera.h"
#include "Sphere.h"
#include "Target.h"
#include "glm\glm.hpp"
#include "glm\gtc\quaternion.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtc\type_ptr.hpp"
#include "glm\gtc\matrix_inverse.hpp"
#include "glm\gtx\rotate_vector.hpp" 

ConsoleWindow console;
Shader* myShader; 
Shader* myBasicShader;  ///shader object 
Shader* bulletShader; 

clock_t currentTime = clock();
clock_t prevTime = clock();
double deltaT = 0;

//MODEL LOADING
#include "3DStruct\threeDModel.h"
#include "Obj\OBJLoader.h"

Plane plane;

float amount = 0;
float temp = 0.00002f;
float pi = 3.14;	
bool showCollisions = false;
ThreeDModel model, world;
vector<ThreeDModel*> worldParts = vector<ThreeDModel*>();
vector<ThreeDModel*> planeParts = vector<ThreeDModel*>();
vector<ThreeDModel*> targetParts = vector<ThreeDModel*>();
vector<Target*> targets = vector<Target*>();
ThreeDModel* sky;
ThreeDModel* bullet;
OBJLoader objLoader;
///END MODEL LOADING

glm::mat4 ProjectionMatrix; // matrix for the orthographic projection
glm::mat4 ModelViewMatrix;  // matrix for the modelling and viewing

//Material properties
float Material_Ambient[4] = {0.4, 0.4, 0.4, 1.0};
float Material_Diffuse[4] = {0.8, 0.9, 0.8, 1.0};
float Material_Specular[4] = {0.8,0.8,0.8,1.0};
float Material_Shininess = 50;

//Light Properties
float Light_Ambient_And_Diffuse[4] = {0.8, 0.8, 0.8, 1.0};
float Light_Specular[4] = {0.6,0.6,0.6,1.0};
float LightPos[4] = {0.8, 1.0, 0.0, 0.0};

float day[4] = {0.8, 0.8, 0.8, 1.0};
float morning[4] = {0.53, 0.63, 0.88, 1.0};
float evening[4] = {0.58, 0.28, 0.47, 1.0};
float night[4] = {0.0, 0.1, 0.2, 1.0};
float currentLight[4] = {0.8,0.8,0.8,1.0};
//
int	mouse_x=0, mouse_y=0;
bool LeftPressed = false;
int screenWidth=1200, screenHeight=800;
bool keys[256];
float spin=0;
float speed=0;

Camera cam1stPerson, cam3rdPerson, camTail, camWing, camWheels;
Camera* closestStaticCam;
vector<Camera*> staticCams;
Camera* activeCam;
//OPENGL FUNCTION PROTOTYPES
void display(float deltaT);				//called in winmain to draw everything to the screen
void reshape();				//called when the window is resized
void init();				//called in winmain when the program starts.
void processKeys();         //called in winmain to process keyboard input
void update();				//called in winmain to update variables
Camera* getClosestStaticCam();
ThreeDModel* loadModel(char* file, Shader* shader);
enum timeOfDay{MORNING,DAY,EVENING,NIGHT};

timeOfDay tod = DAY;

/*************    START OF OPENGL FUNCTIONS   ****************/
void display()									
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);

	glUseProgram(myShader->handle());  // use the shader

	amount += temp;
	if(amount > 1 || amount < -0.5)
		temp = -temp;
	//amount = 0;
	glUniform1f(glGetUniformLocation(myShader->handle(), "displacement"), amount);

	GLuint matLocation = glGetUniformLocation(myShader->handle(), "ProjectionMatrix");  
	glUniformMatrix4fv(matLocation, 1, GL_FALSE, &ProjectionMatrix[0][0]);
	//glm::mat4 viewMatrix = glm::lookAt(plane.getCentre() + plane.getUp() * 6.0f + plane.getForward()*5.0f,plane.getCentre() + plane.getForward()*100.0f,plane.getUp());
	//glm::mat4 viewMatrix = glm::lookAt((plane.getCentre() - plane.getForward() * 100.0f + plane.getUp() * 30.0f),plane.getCentre() ,plane.getUp());
	glm::mat4 viewMatrix = glm::lookAt(activeCam->getPos(),activeCam->gettarget(),activeCam->getUp());
	glUniformMatrix4fv(glGetUniformLocation(myShader->handle(), "ViewMatrix"), 1, GL_FALSE, &viewMatrix[0][0]);
	glm::mat4 frameLightMat = glm::rotate(glm::mat4(1.0),spin,glm::vec3(0,1,0));
	glm::vec4 frameLightPos = frameLightMat * glm::vec4(LightPos[0],LightPos[1],LightPos[2],LightPos[3]);
	float flp[4] = {frameLightPos.x,frameLightPos.y, frameLightPos.z,frameLightPos.w};

	glUniform4fv(glGetUniformLocation(myShader->handle(), "LightPos"), 1, flp);
	//glUniform4fv(glGetUniformLocation(myShader->handle(), "light_ambient"), 1, Light_Ambient_And_Diffuse);
	//glUniform4fv(glGetUniformLocation(myShader->handle(), "light_diffuse"), 1, Light_Ambient_And_Diffuse);
	glUniform4fv(glGetUniformLocation(myShader->handle(), "light_ambient"), 1, currentLight);
	glUniform4fv(glGetUniformLocation(myShader->handle(), "light_diffuse"), 1, currentLight);
	glUniform4fv(glGetUniformLocation(myShader->handle(), "light_specular"), 1, Light_Specular);

	glUniform4fv(glGetUniformLocation(myShader->handle(), "material_ambient"), 1, Material_Ambient);
	glUniform4fv(glGetUniformLocation(myShader->handle(), "material_diffuse"), 1, Material_Diffuse);
	glUniform4fv(glGetUniformLocation(myShader->handle(), "material_specular"), 1, Material_Specular);
	Material_Shininess = 0.5;
	glUniform1f(glGetUniformLocation(myShader->handle(), "material_shininess"), Material_Shininess);
	

	//DRAW THE MODEL
	//GLuint matLocation = glGetUniformLocation(myShader.handle(), "ProjectionMatrix");  
	//glUniformMatrix4fv(matLocation, 1, GL_FALSE, &ProjectionMatrix[0][0]);
	//glm::quat quaternion = glm::quat(glm::vec3(plane.rollVal,plane.yawVal,plane.pitchVal));
	ModelViewMatrix = glm::translate(viewMatrix,plane.getCentre());	//viewing is simple translate

	ModelViewMatrix = ModelViewMatrix * glm::mat4_cast(plane.qorientation);


	glUniformMatrix4fv(glGetUniformLocation(myShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);

	glm::mat3 normalMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));
	glUniformMatrix3fv(glGetUniformLocation(myShader->handle(), "NormalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);

	//model.drawElementsUsingVBO(myShader);
	//for each(ThreeDModel* m in planeParts){
		//m->drawElementsUsingVBO(myShader);
	//}
	plane.draw(myShader);
	//s.render();
	if(showCollisions){	
		glUseProgram(myBasicShader->handle());  // use the shader
		glUniformMatrix4fv(glGetUniformLocation(myBasicShader->handle(), "ProjectionMatrix"), 1, GL_FALSE, &ProjectionMatrix[0][0]);
		glUniformMatrix4fv(glGetUniformLocation(myBasicShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
		//model.drawOctreeLeaves(myBasicShader);
		for each(ThreeDModel* m in planeParts){
			m->drawBoundingBox(myBasicShader);
		}
		
	}
	//prop
	glUseProgram(myShader->handle());  // use the shader

	//All of our geometry will have the same projection matrix.
	//we only need to set it once, since we are using the same shader.
	matLocation = glGetUniformLocation(myShader->handle(), "ProjectionMatrix");  
	glUniformMatrix4fv(matLocation, 1, GL_FALSE, &ProjectionMatrix[0][0]);

	ModelViewMatrix = glm::translate(viewMatrix,plane.getCentre());	//viewing is simple translate

	ModelViewMatrix = ModelViewMatrix * glm::mat4_cast(plane.qorientation);
	
	ModelViewMatrix = glm::rotate(ModelViewMatrix, plane.propRot,glm::vec3(0,0,1));

	glUniformMatrix4fv(glGetUniformLocation(myShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);

	normalMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));
	glUniformMatrix3fv(glGetUniformLocation(myShader->handle(), "NormalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);
	plane.drawProp(myShader);

	for each(Target* t in targets){
		for(int i = 0; i < 4; i++){
			if(t->getActiveParts()[i]){
				glUseProgram(myShader->handle());  // use the shader

				//All of our geometry will have the same projection matrix.
				//we only need to set it once, since we are using the same shader.
				matLocation = glGetUniformLocation(myShader->handle(), "ProjectionMatrix");  
				glUniformMatrix4fv(matLocation, 1, GL_FALSE, &ProjectionMatrix[0][0]);

				ModelViewMatrix = glm::translate(viewMatrix,t->getPos());	//viewing is simple translate

				ModelViewMatrix = ModelViewMatrix * t->getOrientation();

				glUniformMatrix4fv(glGetUniformLocation(myShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);

				normalMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));
				glUniformMatrix3fv(glGetUniformLocation(myShader->handle(), "NormalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);
				t->getParts()[i]->drawElementsUsingVBO(myShader);
			}
		}
	}

	//bullets
	for each(Bullet* b in plane.getFiredBullets()){
	glUseProgram(bulletShader->handle());  // use the shader


	glUniform4fv(glGetUniformLocation(bulletShader->handle(), "LightPos"), 1, flp);
	glUniform4fv(glGetUniformLocation(bulletShader->handle(), "light_ambient"), 1, Light_Ambient_And_Diffuse);
	glUniform4fv(glGetUniformLocation(bulletShader->handle(), "light_diffuse"), 1, Light_Ambient_And_Diffuse);
	glUniform4fv(glGetUniformLocation(bulletShader->handle(), "light_specular"), 1, Light_Specular);

	glUniform4fv(glGetUniformLocation(bulletShader->handle(), "material_ambient"), 1, Material_Ambient);
	glUniform4fv(glGetUniformLocation(bulletShader->handle(), "material_diffuse"), 1, Material_Diffuse);
	glUniform4fv(glGetUniformLocation(bulletShader->handle(), "material_specular"), 1, Material_Specular);
	Material_Shininess = 0.5;
	glUniform1f(glGetUniformLocation(bulletShader->handle(), "material_shininess"), Material_Shininess);
	
		//All of our geometry will have the same projection matrix.
		//we only need to set it once, since we are using the same shader.
		matLocation = glGetUniformLocation(bulletShader->handle(), "ProjectionMatrix");  
		glUniformMatrix4fv(matLocation, 1, GL_FALSE, &ProjectionMatrix[0][0]);

		ModelViewMatrix = glm::translate(viewMatrix,b->getPos());	//viewing is simple translate
	
		ModelViewMatrix = ModelViewMatrix * b->getOrientaion();

		//ModelViewMatrix = glm::scale(ModelViewMatrix,glm::vec3(2,2,10));

		glUniformMatrix4fv(glGetUniformLocation(bulletShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);

		normalMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));
		glUniformMatrix3fv(glGetUniformLocation(bulletShader->handle(), "NormalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);
		glUniform1f(glGetUniformLocation(bulletShader->handle(), "ttl"), b->getTTL());
		b->draw(bulletShader);
	}
	//world
	glUseProgram(myShader->handle());  // use the shader
	Material_Shininess = 10;
	glUniform1f(glGetUniformLocation(myShader->handle(), "material_shininess"), Material_Shininess);
	//All of our geometry will have the same projection matrix.
	//we only need to set it once, since we are using the same shader.
	matLocation = glGetUniformLocation(myShader->handle(), "ProjectionMatrix");  
	glUniformMatrix4fv(matLocation, 1, GL_FALSE, &ProjectionMatrix[0][0]);

	//ModelViewMatrix = glm::translate(,glm::vec3(0,0,0));	//viewing is simple translate
	ModelViewMatrix = viewMatrix;

	glUniformMatrix4fv(glGetUniformLocation(myShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);

	normalMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));
	glUniformMatrix3fv(glGetUniformLocation(myShader->handle(), "NormalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);
	//sky->drawElementsUsingVBO(myShader);
	//world.drawElementsUsingVBO(myShader);
	for each(ThreeDModel* m in worldParts){
		m->drawElementsUsingVBO(myShader);
	}
	if(showCollisions){	
		glUseProgram(myBasicShader->handle());  // use the shader
		glUniformMatrix4fv(glGetUniformLocation(myBasicShader->handle(), "ProjectionMatrix"), 1, GL_FALSE, &ProjectionMatrix[0][0]);
		glUniformMatrix4fv(glGetUniformLocation(myBasicShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
		//model.drawOctreeLeaves(myBasicShader);
		for each(ThreeDModel* m in worldParts){
			m->drawOctreeLeaves(myBasicShader);
			m->drawBoundingBox(myBasicShader);
		}
	}
	//world.drawBoundingBox(myBasicShader);
	//world.drawOctreeLeaves(myBasicShader);

	glUseProgram(myShader->handle());  // use the shader

	//All of our geometry will have the same projection matrix.
	//we only need to set it once, since we are using the same shader.
	Material_Shininess = 10;
	glUniform1f(glGetUniformLocation(myShader->handle(), "material_shininess"), Material_Shininess);
	matLocation = glGetUniformLocation(myShader->handle(), "ProjectionMatrix");  
	glUniformMatrix4fv(matLocation, 1, GL_FALSE, &ProjectionMatrix[0][0]);

	//ModelViewMatrix = glm::translate(,glm::vec3(0,0,0));	//viewing is simple translate
	
	ModelViewMatrix = glm::rotate(viewMatrix,spin,glm::vec3(0,1,0));
	
	glUniformMatrix4fv(glGetUniformLocation(myShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);

	normalMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));
	glUniformMatrix3fv(glGetUniformLocation(myShader->handle(), "NormalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);
	sky->drawElementsUsingVBO(myShader);
	glUseProgram(0); //turn off the current shader
	glFlush();
}

void reshape(int width, int height)		// Resize the OpenGL window
{
	screenWidth=width; screenHeight = height;           // to ensure the mouse coordinates match 
														// we will use these values to set the coordinate system

	glViewport(0,0,width,height);						// Reset The Current Viewport

	//Set the projection matrix
	ProjectionMatrix = glm::perspective(60.0f, (GLfloat)screenWidth/(GLfloat)screenHeight, 1.0f, 200000.0f);
}
void init()
{
	glClearColor(1.0,1.0,1.0,0.0);						//sets the clear colour to yellow
														//glClear(GL_COLOR_BUFFER_BIT) in the display function
														//will clear the buffer to this colour
	glEnable(GL_DEPTH_TEST);

	myBasicShader = new Shader;
    if(!myBasicShader->load("BasicView", "glslfiles/basic.vert", "glslfiles/basic.frag"))
	{
		cout << "failed to load shader" << endl;
	}

	myShader = new Shader;
	//if(!myShader->load("BasicView", "glslfiles/basicTransformationsWithDisplacement.vert", "glslfiles/basicTransformationsWithDisplacement.frag"))
    if(!myShader->load("BasicView", "glslfiles/basicTransformations.vert", "glslfiles/basicTransformations.frag"))
	{
		cout << "failed to load shader" << endl;
	}

	bulletShader = new Shader;
    if(!bulletShader->load("BasicView", "glslfiles/bulletShader.vert", "glslfiles/bulletShader.frag"))
	{
		cout << "failed to load shader" << endl;
	}

	glUseProgram(myShader->handle());  // use the shader

	

	glEnable(GL_TEXTURE_2D);

	planeParts.push_back(loadModel("TestModels/bodyFront.obj",myShader));
	planeParts.push_back(loadModel("TestModels/wingRight.obj",myShader));
	planeParts.push_back(loadModel("TestModels/wingLeft.obj",myShader));
	planeParts.push_back(loadModel("TestModels/wheels.obj",myShader));
	planeParts.push_back(loadModel("TestModels/prop.obj",myShader));
	planeParts.push_back(loadModel("TestModels/planeRear.obj",myShader));
	planeParts.push_back(loadModel("TestModels/gun.obj",myShader));
	sky = loadModel("TestModels/sky.obj",myShader);
	worldParts.push_back(loadModel("TestModels/floor2.obj",myShader));
	worldParts.push_back(loadModel("TestModels/platTop2.obj",myShader));
	worldParts.push_back(loadModel("TestModels/platWalls2.obj",myShader));	
	worldParts.push_back(loadModel("TestModels/walls12.obj",myShader));
	worldParts.push_back(loadModel("TestModels/walls22.obj",myShader));
	worldParts.push_back(loadModel("TestModels/walls32.obj",myShader));
	worldParts.push_back(loadModel("TestModels/walls42.obj",myShader));
	worldParts.push_back(loadModel("TestModels/cliffTop12.obj",myShader));
	worldParts.push_back(loadModel("TestModels/cliffTop22.obj",myShader));
	worldParts.push_back(loadModel("TestModels/cliffTop32.obj",myShader));
	worldParts.push_back(loadModel("TestModels/hanger.obj",myShader));
	worldParts.push_back(loadModel("TestModels/block1.obj",myShader));
	worldParts.push_back(loadModel("TestModels/block2.obj",myShader));
	worldParts.push_back(loadModel("TestModels/block3.obj",myShader));
	worldParts.push_back(loadModel("TestModels/block4.obj",myShader));
	worldParts.push_back(loadModel("TestModels/block5.obj",myShader));
	worldParts.push_back(loadModel("TestModels/tunnel.obj",myShader));
	worldParts.push_back(loadModel("TestModels/torus.obj",myShader));

	targetParts.push_back(loadModel("TestModels/target1.obj",myShader));
	targetParts.push_back(loadModel("TestModels/target2.obj",myShader));
	targetParts.push_back(loadModel("TestModels/target3.obj",myShader));
	targetParts.push_back(loadModel("TestModels/target4.obj",myShader));

	for(int i = 0; i < 25; i ++)
		targets.push_back(new Target(targetParts,i*50));


	bullet = loadModel("TestModels/bullet.obj", bulletShader);
	bullet->calcCentrePoint();
	bullet->centreOnZero();

	plane = Plane(planeParts,glm::vec3(-5336,-4583,3451), bullet);
	cam1stPerson = Camera(glm::vec3(0,0,0),&plane,CamType::FIRST_PERSON);
	cam3rdPerson = Camera(glm::vec3(0,0,0),&plane, CamType::THIRD_PERSON);
	camTail = Camera(glm::vec3(0,0,0),&plane, CamType::TAIL);
	camWheels = Camera(glm::vec3(0,0,0),&plane, CamType::WHEEL);
	camWing = Camera(glm::vec3(0,0,0),&plane, CamType::WING);
	staticCams = vector<Camera*>();
	for(int x = -10000; x <= 10000; x+=10000){
		for(int z = -10000;z <= 10000; z+=10000){
			for(int y = -5000; y <= 0; y+= 5000){
				staticCams.push_back(new Camera(glm::vec3(x,y,z),&plane, CamType::STATIC));
			}
		}
	}
	closestStaticCam = getClosestStaticCam();
	activeCam = &cam3rdPerson;
}
void processKeys()
{
	float deltaS = 75 * deltaT;
	float angle = 0.25 * deltaT;
	float r = 0; 
	float p = 0; 
	float y = 0;
	if (keys[VK_LEFT])
	{
		if( plane.getSpeed() > 200 && plane.rightWing)
			r -= angle * (plane.getSpeed()+1)/200;
		//keys[VK_LEFT] = false;
		
	}
	if (keys[VK_RIGHT])
	{
		if(plane.getSpeed() > 200 && plane.leftWing)
			r += angle * (plane.getSpeed()+1)/200;
		//keys[VK_RIGHT] = false;
	}
	if (keys[VK_UP])
	{
		if(plane.getSpeed() > 200 && (plane.leftWing || plane.rightWing))
			p += angle;
		//keys[VK_UP] = false;

	}
	if (keys[VK_DOWN])
	{
		if(plane.getSpeed()>200 && (plane.leftWing || plane.rightWing))
			p -= angle * plane.getSpeed()/400;
		//keys[VK_DOWN] = false;
	}
	if (keys['A'])
	{
		if(plane.rear)
			y += angle;
		//keys['A'] = false;
	}
	if (keys['D'])
	{
		if(plane.rear)
			y -= angle;
		//keys['D'] = false;
	}
	if (keys['S'])
	{
		plane.setSpeed(plane.getSpeed() - 2*deltaS);
		//keys['S'] = false;
	}
	if (keys['W'])
	{
		if(plane.getSpeed() < 1000)
			plane.setSpeed(plane.getSpeed() + deltaS);
		//keys['W'] = false;
	}
	else
		plane.setSpeed(plane.getSpeed() - deltaS/4);

	if(keys['F'])
		plane.reload(bullet);
	if(keys[VK_SPACE])
		plane.fire();
	if(keys['1'])
		activeCam = &cam1stPerson;
	if(keys['2'])
		activeCam = closestStaticCam;
	if(keys['3']){
		activeCam = &cam3rdPerson;
		activeCam->setPos(plane.getCentre() - plane.getForward()*200.0f);
	}
	if(keys['4'])
		activeCam = &camTail;
	if(keys['5'])
		activeCam = &camWing;
	if(keys['6'])
		activeCam = &camWheels;
	if(keys['C']){
		showCollisions = !showCollisions;
		keys['C'] = false;
	}
	if(keys['R']){
		if(plane.crashed){
			plane.respawn();
			cam3rdPerson.setPos(plane.getCentre() - plane.getForward()*100.0f);
		}
	}
	if(!plane.crashed){
		if(!plane.leftWing)
			r -= angle * plane.getSpeed()/200;
		if(!plane.rightWing)
			r += angle * plane.getSpeed()/200;
		//if(!plane.rightWing && !plane.leftWing){
		
			//r += angle * plane.getSpeed()/400;
		//}
	
		glm::vec3 frameRotAngles = glm::vec3(p, y, r);
		glm::vec3 framePropRotAng = glm::vec3(p,y,r + plane.getSpeed() *deltaT);
		glm::quat frameRot = glm::quat(frameRotAngles);
		glm::quat propFrameRot = glm::quat(framePropRotAng);
		plane.qorientation = plane.qorientation * frameRot;
		plane.propQOrientation = plane.propQOrientation * propFrameRot;
	}
}
void update(float deltaT)
{
	if(deltaT > 0.03)
		deltaT = 0.03;
	spin += 1*deltaT;
	plane.update(deltaT);
	plane.collisionDetection(worldParts);
	int count = 0;
	for each(Target* t in targets){
		count++;
		t->update(deltaT,&plane,plane.getFiredBullets(),count);
	}

	if(activeCam->type == STATIC){
		closestStaticCam = getClosestStaticCam();
		activeCam = closestStaticCam;
	}

	activeCam->update(deltaT);

	float diff[4];
	float diffVal;
	switch(tod){
	case MORNING:
		for(int i = 0; i < 4; i++){
			currentLight[i] += (day[i] - morning[i]) * (deltaT/30);
			diff[i] = day[i]-currentLight[i];
		}
		diffVal = diff[0]*diff[0] + diff[1]*diff[1] + diff[2]*diff[2];
		if(diffVal < 0.001){
			tod = DAY;
			for(int i = 0; i < 4; i++)
				currentLight[i] = day[i];
		}
		break;
	case DAY:
		for(int i = 0; i < 4; i++){
			currentLight[i] += (evening[i] - day[i]) * (deltaT/30);
			diff[i] = evening[i]-currentLight[i];
		}
		diffVal = diff[0]*diff[0] + diff[1]*diff[1] + diff[2]*diff[2];
		if(diffVal < 0.001){
			tod = EVENING;
			for(int i = 0; i < 4; i++)
				currentLight[i] = evening[i];
		}
		break;
	case EVENING:
		for(int i = 0; i < 4; i++){
			currentLight[i] += (night[i] - evening[i]) * (deltaT/30);
			diff[i] = night[i]-currentLight[i];
		}
		diffVal = diff[0]*diff[0] + diff[1]*diff[1] + diff[2]*diff[2];
		if(diffVal < 0.001){
			tod = NIGHT;
			for(int i = 0; i < 4; i++)
				currentLight[i] = night[i];
		}
		break;
	case NIGHT:
		for(int i = 0; i < 4; i++){
			currentLight[i] += (morning[i] - night[i]) * (deltaT/30);
			diff[i] = morning[i]-currentLight[i];
		}
		diffVal = diff[0]*diff[0] + diff[1]*diff[1] + diff[2]*diff[2];
		if(diffVal < 0.001){
			tod = MORNING;
			for(int i = 0; i < 4; i++)
				currentLight[i] = morning[i];
		}
		break;
	}

}

float absolute(glm::vec3 p){
	return p.x*p.x + p.y*p.y + p.z*p.z; 
}

Camera* getClosestStaticCam(){
	Camera* closest = staticCams[0];
	float minDist = absolute(closest->getPos()/1000.0f - plane.getCentre()/1000.0f);
	for each(Camera* c in staticCams){	
		float dist = absolute(c->getPos()/1000.0f - plane.getCentre()/1000.0f);
		if(dist < minDist){
			minDist = dist;
			closest = c;
		}
	}
	
	return closest;
}

ThreeDModel* loadModel(char* file, Shader* myShader){
	ThreeDModel* model = new ThreeDModel();
	cout << " loading model " << endl;
	if(objLoader.loadModel(file, *model))//returns true if the model is loaded, puts the model in the model parameter
	{
		cout << " model loaded " << endl;		

		//if you want to translate the object to the origin of the screen,
		//first calculate the centre of the object, then move all the vertices
		//back so that the centre is on the origin.
		//model->calcCentrePoint();
		//model->centreOnZero();

	
		model->calcVertNormalsUsingOctree();  //the method will construct the octree if it hasn't already been created.
				

		//turn on VBO by setting useVBO to true in threeDmodel.cpp default constructor - only permitted on 8 series cards and higher
		model->initDrawElements();
		model->initVBO(myShader);
		//model->deleteVertexFaceData();
		
	}
	else
	{
		cout << " model failed to load " << endl;
	}

	return model;
} 
/**************** END OPENGL FUNCTIONS *************************/

//WIN32 functions
LRESULT	CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);	// Declaration For WndProc
void KillGLWindow();									// releases and destroys the window
bool CreateGLWindow(char* title, int width, int height); //creates the window
int WINAPI WinMain(	HINSTANCE, HINSTANCE, LPSTR, int);  // Win32 main function

//win32 global variabless
HDC			hDC=NULL;		// Private GDI Device Context
HGLRC		hRC=NULL;		// Permanent Rendering Context
HWND		hWnd=NULL;		// Holds Our Window Handle
HINSTANCE	hInstance;		// Holds The Instance Of The Application


/******************* WIN32 FUNCTIONS ***************************/
int WINAPI WinMain(	HINSTANCE	hInstance,			// Instance
					HINSTANCE	hPrevInstance,		// Previous Instance
					LPSTR		lpCmdLine,			// Command Line Parameters
					int			nCmdShow)			// Window Show State
{
	MSG		msg;									// Windows Message Structure
	bool	done=false;								// Bool Variable To Exit Loop

	console.Open();


	// Create Our OpenGL Window
	if (!CreateGLWindow("OpenGL Win32 Example",screenWidth,screenHeight))
	{
		return 0;									// Quit If Window Was Not Created
	}

	while(!done)									// Loop That Runs While done=FALSE
	{
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))	// Is There A Message Waiting?
		{
			if (msg.message==WM_QUIT)				// Have We Received A Quit Message?
			{
				done=true;							// If So done=TRUE
			}
			else									// If Not, Deal With Window Messages
			{
				TranslateMessage(&msg);				// Translate The Message
				DispatchMessage(&msg);				// Dispatch The Message
			}
		}
		else										// If There Are No Messages
		{
			if(keys[VK_ESCAPE])
				done = true;

			currentTime = clock();
			clock_t clockTicksTaken = currentTime - prevTime;
			deltaT =(clockTicksTaken / (double) CLOCKS_PER_SEC);

			processKeys();			//process keyboard
			update(deltaT);					// update variables
			display();					// Draw The Scene
			

			prevTime = currentTime;

			SwapBuffers(hDC);				// Swap Buffers (Double Buffering)
		}
	}

	console.Close();

	// Shutdown
	KillGLWindow();									// Kill The Window
	return (int)(msg.wParam);						// Exit The Program
}

//WIN32 Processes function - useful for responding to user inputs or other events.
LRESULT CALLBACK WndProc(	HWND	hWnd,			// Handle For This Window
							UINT	uMsg,			// Message For This Window
							WPARAM	wParam,			// Additional Message Information
							LPARAM	lParam)			// Additional Message Information
{
	switch (uMsg)									// Check For Windows Messages
	{
		case WM_CLOSE:								// Did We Receive A Close Message?
		{
			PostQuitMessage(0);						// Send A Quit Message
			return 0;								// Jump Back
		}
		break;

		case WM_SIZE:								// Resize The OpenGL Window
		{
			reshape(LOWORD(lParam),HIWORD(lParam));  // LoWord=Width, HiWord=Height
			return 0;								// Jump Back
		}
		break;

		case WM_LBUTTONDOWN:
			{
	            mouse_x = LOWORD(lParam);          
				mouse_y = screenHeight - HIWORD(lParam);
				LeftPressed = true;
			}
		break;

		case WM_LBUTTONUP:
			{
	            LeftPressed = false;
			}
		break;

		case WM_MOUSEMOVE:
			{
	            mouse_x = LOWORD(lParam);          
				mouse_y = screenHeight  - HIWORD(lParam);
			}
		break;
		case WM_KEYDOWN:							// Is A Key Being Held Down?
		{
			keys[wParam] = true;					// If So, Mark It As TRUE
			return 0;								// Jump Back
		}
		break;
		case WM_KEYUP:								// Has A Key Been Released?
		{
			keys[wParam] = false;					// If So, Mark It As FALSE
			return 0;								// Jump Back
		}
		break;
	}

	// Pass All Unhandled Messages To DefWindowProc
	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}

void KillGLWindow()								// Properly Kill The Window
{
	if (hRC)											// Do We Have A Rendering Context?
	{
		if (!wglMakeCurrent(NULL,NULL))					// Are We Able To Release The DC And RC Contexts?
		{
			MessageBox(NULL,"Release Of DC And RC Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(hRC))						// Are We Able To Delete The RC?
		{
			MessageBox(NULL,"Release Rendering Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}
		hRC=NULL;										// Set RC To NULL
	}

	if (hDC && !ReleaseDC(hWnd,hDC))					// Are We Able To Release The DC
	{
		MessageBox(NULL,"Release Device Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hDC=NULL;										// Set DC To NULL
	}

	if (hWnd && !DestroyWindow(hWnd))					// Are We Able To Destroy The Window?
	{
		MessageBox(NULL,"Could Not Release hWnd.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hWnd=NULL;										// Set hWnd To NULL
	}

	if (!UnregisterClass("OpenGL",hInstance))			// Are We Able To Unregister Class
	{
		MessageBox(NULL,"Could Not Unregister Class.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hInstance=NULL;									// Set hInstance To NULL
	}
}

/*	This Code Creates Our OpenGL Window.  Parameters Are:					*
 *	title			- Title To Appear At The Top Of The Window				*
 *	width			- Width Of The GL Window Or Fullscreen Mode				*
 *	height			- Height Of The GL Window Or Fullscreen Mode			*/
 
bool CreateGLWindow(char* title, int width, int height)
{
	GLuint		PixelFormat;			// Holds The Results After Searching For A Match
	WNDCLASS	wc;						// Windows Class Structure
	DWORD		dwExStyle;				// Window Extended Style
	DWORD		dwStyle;				// Window Style
	RECT		WindowRect;				// Grabs Rectangle Upper Left / Lower Right Values
	WindowRect.left=(long)0;			// Set Left Value To 0
	WindowRect.right=(long)width;		// Set Right Value To Requested Width
	WindowRect.top=(long)0;				// Set Top Value To 0
	WindowRect.bottom=(long)height;		// Set Bottom Value To Requested Height

	hInstance			= GetModuleHandle(NULL);				// Grab An Instance For Our Window
	wc.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// Redraw On Size, And Own DC For Window.
	wc.lpfnWndProc		= (WNDPROC) WndProc;					// WndProc Handles Messages
	wc.cbClsExtra		= 0;									// No Extra Window Data
	wc.cbWndExtra		= 0;									// No Extra Window Data
	wc.hInstance		= hInstance;							// Set The Instance
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);			// Load The Default Icon
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	wc.hbrBackground	= NULL;									// No Background Required For GL
	wc.lpszMenuName		= NULL;									// We Don't Want A Menu
	wc.lpszClassName	= "OpenGL";								// Set The Class Name

	if (!RegisterClass(&wc))									// Attempt To Register The Window Class
	{
		MessageBox(NULL,"Failed To Register The Window Class.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;											// Return FALSE
	}
	
	dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			// Window Extended Style
	dwStyle=WS_OVERLAPPEDWINDOW;							// Windows Style
	
	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);		// Adjust Window To True Requested Size

	// Create The Window
	if (!(hWnd=CreateWindowEx(	dwExStyle,							// Extended Style For The Window
								"OpenGL",							// Class Name
								title,								// Window Title
								dwStyle |							// Defined Window Style
								WS_CLIPSIBLINGS |					// Required Window Style
								WS_CLIPCHILDREN,					// Required Window Style
								0, 0,								// Window Position
								WindowRect.right-WindowRect.left,	// Calculate Window Width
								WindowRect.bottom-WindowRect.top,	// Calculate Window Height
								NULL,								// No Parent Window
								NULL,								// No Menu
								hInstance,							// Instance
								NULL)))								// Dont Pass Anything To WM_CREATE
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Window Creation Error.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	static	PIXELFORMATDESCRIPTOR pfd=				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		24,										// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		24,											// 24Bit Z-Buffer (Depth Buffer)  
		0,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};
	
	if (!(hDC=GetDC(hWnd)))							// Did We Get A Device Context?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Create A GL Device Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!(PixelFormat=ChoosePixelFormat(hDC,&pfd)))	// Did Windows Find A Matching Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Find A Suitable PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if(!SetPixelFormat(hDC,PixelFormat,&pfd))		// Are We Able To Set The Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Set The PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	HGLRC tempContext = wglCreateContext(hDC);
	wglMakeCurrent(hDC, tempContext);

	glewInit();

	int attribs[] =
	{
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
		WGL_CONTEXT_MINOR_VERSION_ARB, 1,
		WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
		0
	};
	
    if(wglewIsSupported("WGL_ARB_create_context") == 1)
    {
		hRC = wglCreateContextAttribsARB(hDC,0, attribs);
		wglMakeCurrent(NULL,NULL);
		wglDeleteContext(tempContext);
		wglMakeCurrent(hDC, hRC);
	}
	else
	{	//It's not possible to make a GL 3.x context. Use the old style context (GL 2.1 and before)
		hRC = tempContext;
		cout << " not possible to make context "<< endl;
	}

	//Checking GL version
	const GLubyte *GLVersionString = glGetString(GL_VERSION);

	cout << GLVersionString << endl;

	//OpenGL 3.2 way of checking the version
	int OpenGLVersion[2];
	glGetIntegerv(GL_MAJOR_VERSION, &OpenGLVersion[0]);
	glGetIntegerv(GL_MINOR_VERSION, &OpenGLVersion[1]);

	cout << OpenGLVersion[0] << " " << OpenGLVersion[1] << endl;

	ShowWindow(hWnd,SW_SHOW);						// Show The Window
	SetForegroundWindow(hWnd);						// Slightly Higher Priority
	SetFocus(hWnd);									// Sets Keyboard Focus To The Window
	reshape(width, height);					// Set Up Our Perspective GL Screen

	init();
	
	return true;									// Success
}



