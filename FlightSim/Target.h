#pragma once
#include "glm\glm.hpp"
#include "Plane.h"
#include "Bullet.h"
#include "Time.h"
#include "Math.h"

class Target{
private:
	vector<ThreeDModel*> parts;
	bool activeParts[4];
	float ttl;
	glm::vec3 pos;
	glm:: mat4 orientation;
public:
	Target(){};
	Target(vector<ThreeDModel*> p, int seed);
	void Collisions(Plane* plane, vector<Bullet*> bullets);
	bool destroyed();
	void respawn(int seed);
	void update(float deltaT, Plane* p, vector<Bullet*> b, int seed);
	vector<ThreeDModel*> getParts(){return parts;}
	bool* getActiveParts(){return activeParts;}
	glm::vec3 getPos(){return pos;}
	glm::mat4 getOrientation(){return orientation;}
};