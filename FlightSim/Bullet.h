#pragma once
#include "glm\glm.hpp"
#include "Collisions.h"
#include "3dStruct\threeDModel.h"

class Bullet{
private:
	glm::vec3 dir;
	glm::vec3 pos;
	glm::mat4 orientation;
	
	float speed;
	float ttl;
public:
	Bullet(){}
	Bullet(ThreeDModel* b);
	void setDir(glm::vec3 d){dir = d;}
	void setPos(glm::vec3 p){pos = p;}
	void setOrientaion(glm::mat4 o){orientation = o;}
	glm::vec3 getPos(){return pos;}
	glm::mat4 getOrientaion(){return orientation;}
	void setSpeed(float s){speed = s;}
	float getTTL(){return ttl;}
	void update(float deltaT);
	void draw(Shader* s);
	
	ThreeDModel* bullet;
};