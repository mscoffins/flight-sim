#include "Target.h"

Target::Target(vector<ThreeDModel*> p, int seed){
	parts = p;
	ttl = 120;
	srand(time(NULL) + seed);
	rand();
	float x = rand()%20000;
	x = x - 10000;
	float y = rand()%7500;
	y = y - 5000;
	float z = rand()%20000;
	z = z - 10000;

	pos = glm::vec3(x,y,z);
	
	float pitch = rand()%180;
	pitch = pitch - 90;
	float yaw = rand()%180;
	yaw = yaw - 90;
	float roll = rand()%180;
	roll = roll - 90;

	glm::quat q = glm::quat(glm::vec3(pitch, yaw, roll));
	orientation = mat4_cast(q);

	for(int i = 0; i < 4; i++){
		activeParts[i] = true;
	}


}
void Target::Collisions(Plane* plane, vector<Bullet*> bullets){
	glm::mat4 pptrans = glm::translate(glm::mat4(1.0), plane->getCentre());
	glm::mat4 ppmm = pptrans * glm::mat4_cast(plane->qorientation);
	for(int i = 0; i < 4; i++){
		if(activeParts[i]){
			vec3* pbbox = parts[i]->octree->getBBVerts();
			glm::mat4 trans = glm::translate(glm::mat4(1.0), pos);
			glm::mat4 mm = trans * orientation;
			for(int x = 0; x < 8; x++)
				pbbox[x] = glm::vec3(mm * glm::vec4(pbbox[x],1));

			vec3* pnorms = Collisions::calcBoxNormals(pbbox);
			for each(Bullet* b in bullets){
				glm::mat4 btrans = glm::translate(glm::mat4(1.0), b->getPos());
				glm::mat4 bmm = btrans * b->getOrientaion();
				vec3* bbbox = b->bullet->octree->getBBVerts();
				for(int b = 0; b < 8; b++)
					bbbox[b] = glm::vec3(bmm * glm::vec4(bbbox[b],1));
				vec3* bnorms = Collisions::calcBoxNormals(bbbox);
				bool col = Collisions::OBBOBBCollision(pbbox,bbbox,pnorms,bnorms);
				if(col){
					activeParts[i] = false;
				}
			}
			
			for each(ThreeDModel* pp in plane->getModel()){
				
				vec3* ppbbox = pp->octree->getBBVerts();
				for(int p = 0; p < 8; p++)
					ppbbox[p] = glm::vec3(ppmm * glm::vec4(ppbbox[p],1));
				vec3* ppnorms = Collisions::calcBoxNormals(ppbbox);
				bool col = Collisions::OBBOBBCollision(pbbox,ppbbox,pnorms,ppnorms);
				if(col){
					activeParts[i] = false;
				}
			}
		}
	}

}

bool Target::destroyed(){
	for(int i = 0; i < 4; i++){
		if(activeParts[i])
			return false;
	}
	return true;
}
void Target::respawn(int seed){
	ttl = 120;
	srand(time(NULL) + seed);
	rand();
	float x = rand()%20000;
	x = x - 10000;
	float y = rand()%7500;
	y = y - 5000;
	float z = rand()%20000;
	z = z - 10000;

	pos = glm::vec3(x,y,z);
	
	float pitch = rand()%180;
	pitch = pitch - 90;
	float yaw = rand()%180;
	yaw = yaw - 90;
	float roll = rand()%180;
	roll = roll - 90;

	glm::quat q = glm::quat(glm::vec3(pitch, yaw, roll));
	orientation = mat4_cast(q);

	for(int i = 0; i < 4; i++){
		activeParts[i] = true;
	}
}

void Target::update(float deltaT, Plane* p, vector<Bullet*> b, int seed){
	ttl-=deltaT;
	if(ttl <=0 || destroyed())
		respawn(seed);
	Collisions(p,b);
}