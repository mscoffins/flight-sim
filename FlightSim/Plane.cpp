#include "Plane.h"

Plane::Plane(){
	
}

Plane::Plane(vector<ThreeDModel*> m, glm::vec3 c, ThreeDModel* b){
	planeParts = m;
	centre = c;
	speed = 0.0;
	qorientation = glm::quat(glm::vec3(0,0,0));
	propQOrientation = glm::quat(glm::vec3(0,0,0));
	dt = 0;
	landed = false;
	leftWing = rightWing = rear = true;
	propRot = 0;
	crashed = false;
	fireDelay = 0;
	bullets = vector<Bullet*>();
	firedBullets = vector<Bullet*>();
	for(int i = 0; i < 50; i++){
		bullets.push_back(new Bullet(b));
	}
}

void Plane::update(float deltaT){
	dt = deltaT;
	if(!crashed){
		if(!leftWing && !rightWing && !rear)
			speed -= 250*deltaT;
		if(speed < 0)
			speed = 0;
		propRot += 4*speed*deltaT;
		if(propRot > 360)
			propRot = 0;
		//std::cout << centre.x << ", " << centre.y << ", "<< centre.z << std::endl;
		centre += this->getForward() * (deltaT * speed);
		centre -= (glm::vec3(0,100,0) * deltaT);
		if(leftWing || rightWing)
			centre += getUp() * speed/10.0f * deltaT;

		fireDelay -= deltaT;
		if(fireDelay < 0)
			fireDelay = 0;

	}
	for each(Bullet* b in firedBullets){
		b->update(deltaT);
	}
	if(firedBullets.size() > 0){
		if(firedBullets.at(0)->getTTL() <= 0)
			firedBullets.erase(firedBullets.begin());
	}
}
void Plane::draw(Shader *myShader){
	planeParts[0]->drawElementsUsingVBO(myShader);
	if(rightWing)
		planeParts[1]->drawElementsUsingVBO(myShader);
	if(leftWing)
		planeParts[2]->drawElementsUsingVBO(myShader);
	planeParts[3]->drawElementsUsingVBO(myShader);
	//planeParts[4]->drawElementsUsingVBO(myShader);
	if(rear)
		planeParts[5]->drawElementsUsingVBO(myShader);

	planeParts[6]->drawElementsUsingVBO(myShader);
}

void Plane::reload(ThreeDModel* b){
	while(bullets.size() <= 50){
		bullets.push_back(new Bullet(b));
		fireDelay+=0.1;
	}
}

void Plane::fire(){
	if(fireDelay == 0 && bullets.size() > 0){
		Bullet* b = bullets.back();
		b->setDir(getForward());
		b->setPos(centre - getUp() * 15.0f + getRight() * 18.0f + getForward() * 85.0f);
		b->setSpeed(4000);
		b->setOrientaion(mat4_cast(qorientation));
		firedBullets.push_back(b);
		bullets.pop_back();
		fireDelay = 0.2;
	}
}

void Plane::drawProp(Shader *myShader){
	planeParts[4]->drawElementsUsingVBO(myShader);
}

glm::vec3 Plane::getUp() {
	glm::mat4 temp = glm::mat4_cast(qorientation);
	return glm::vec3(temp[1][0], temp[1][1], temp[1][2]);
}

glm::vec3 Plane::getRight() {
	glm::mat4 temp = glm::mat4_cast(qorientation);
	return glm::vec3(temp[0][0], temp[0][1], temp[0][2]);
}
glm::vec3 Plane::getForward() {
	glm::mat4 temp = glm::mat4_cast(qorientation);
	return glm::vec3(temp[2][0], temp[2][1], temp[2][2]);
}

void Plane::respawn(){
	centre = glm::vec3(-5336,-4583,3451);
	leftWing = rightWing = rear = true;
	speed = 0;
	qorientation = glm::quat(glm::vec3(0,0,0));
	landed = false;
	crashed = false;
}

void Plane::collisionDetection(vector<ThreeDModel*> world){
	for(int p = 0; p < 6; p++){
		if(!(p==1&&!rightWing) && !(p==2&&!leftWing) && !(p==5&&!rear)){
			glm::mat4 trans = glm::translate(glm::mat4(1.0), centre);
			glm::mat4 rot = glm::mat4_cast(qorientation);
			glm::mat4 modelMat = trans * rot;
			glm::vec3* partVerts = planeParts[p]->octree->getBBVerts();
			for(int i = 0; i < 8; i++){
				partVerts[i] = glm::vec3(modelMat * glm::vec4(partVerts[i], 1.0));
			}
			for each(ThreeDModel* m in world){
				Octree* worldOct = m->octree;
				collidePlanePart(worldOct, m, partVerts,p);
				//collideOctNode(worldOct, m);
			}
		}
	}
	
}

//void Plane::collideOctNode(Octree* oct, ThreeDModel* obj){
//	glm::vec3 bmin = glm::vec3(oct->minX,oct->minY, oct->minZ);
//	glm::vec3 bmax = glm::vec3(oct->maxX,oct->maxY, oct->maxZ);
//	bool col = Collisions::sphereBoxColl(bmin,bmax,centre,s.getRad());
//	if(col){
//		if(oct->getLevel() != MAX_DEPTH){
//		//cout << "COLLISION at " << oct->getLevel() << endl;
//			for(int i = 0; i < 8; i++){
//				Octree* child = oct->getChild(i);
//				if(child != NULL)
//					collideOctNode(child,obj);
//			}
//		}
//		else{
//			//cout << "COLLISION at leaf" << endl;
//			collidePoly(oct, obj);
//
//		}
//	}
//	else{
//		landed = false;
//	}
//}

void Plane::collidePoly(Octree* oct, ThreeDModel* obj){
	
	int* prims = oct->getPrimitiveList();
	int primsLength = oct->getPrimitiveListSize();
	
	for(int i = 0; i < primsLength; i++){
		glm::mat4 trans = glm::translate(glm::mat4(1.0),centre);
		glm::mat4 rot = glm::mat4_cast(qorientation);
		glm::vec3 triVerts[3];

		triVerts[0] = glm::vec3(obj->theVerts[obj->theFaces[prims[i]].thePoints[0]].x,
								obj->theVerts[obj->theFaces[prims[i]].thePoints[0]].y,
								obj->theVerts[obj->theFaces[prims[i]].thePoints[0]].z);

		triVerts[1] = glm::vec3(obj->theVerts[obj->theFaces[prims[i]].thePoints[1]].x, 
								obj->theVerts[obj->theFaces[prims[i]].thePoints[1]].y,
								obj->theVerts[obj->theFaces[prims[i]].thePoints[1]].z);

		triVerts[2] = glm::vec3(obj->theVerts[obj->theFaces[prims[i]].thePoints[2]].x,
								obj->theVerts[obj->theFaces[prims[i]].thePoints[2]].y,
								obj->theVerts[obj->theFaces[prims[i]].thePoints[2]].z);

		glm::vec3 triNormal = Collisions::getTriNormal(triVerts[0], triVerts[1], triVerts[2]);

		glm::vec3* bodyFrontVerts = planeParts[0]->octree->getBBVerts();
		glm::vec3* wingRightVerts = planeParts[1]->octree->getBBVerts();
		glm::vec3* wingLeftVerts = planeParts[2]->octree->getBBVerts();
		glm::vec3* wheelVerts = planeParts[3]->octree->getBBVerts();
		glm::vec3* propVerts = planeParts[4]->octree->getBBVerts();
		glm::vec3* bodyRearVerts = planeParts[5]->octree->getBBVerts();

		float bodyVertsAreas[8];
		float wingLeftVertsAreas[8];
		float wingRightVertsAreas[8];
		float wheelVertsAreas[8];
		float propVertsAreas[8];
		float bodyRearVertsAreas[8];
		glm::mat4 modelMat = trans*rot;
		for(int j = 0; j < 8; j++){
			
			bodyVertsAreas[j] = Collisions::onLeftTri(triVerts[0], triVerts[1], triVerts[2], glm::vec3(modelMat*glm::vec4(bodyFrontVerts[j],1.0)));
			wingRightVertsAreas[j] = Collisions::onLeftTri(triVerts[0], triVerts[1], triVerts[2], glm::vec3(modelMat*glm::vec4(wingRightVerts[j],1.0)));
			wingLeftVertsAreas[j] = Collisions::onLeftTri(triVerts[0], triVerts[1], triVerts[2], glm::vec3(modelMat*glm::vec4(wingLeftVerts[j],1.0)));
			wheelVertsAreas[j] = Collisions::onLeftTri(triVerts[0], triVerts[1], triVerts[2], glm::vec3(modelMat*glm::vec4(wheelVerts[j],1.0)));
			propVertsAreas[j] = Collisions::onLeftTri(triVerts[0], triVerts[1], triVerts[2], glm::vec3(modelMat*glm::vec4(propVerts[j], 1.0)));
			bodyRearVertsAreas[j] = Collisions::onLeftTri(triVerts[0], triVerts[1], triVerts[2], glm::vec3(modelMat*glm::vec4(bodyRearVerts[j],1.0)));
		}

		bool bodyCol, wingRightCol, wingLeftCol, wheelCol, propCol, rearCol;

		bodyCol = Collisions::onSameSide(bodyVertsAreas,8);
		wingRightCol = Collisions::onSameSide(wingRightVertsAreas,8);
		wingLeftCol = Collisions::onSameSide(wingLeftVertsAreas,8);
		wheelCol = Collisions::onSameSide(wheelVertsAreas,8);
		propCol = Collisions::onSameSide(propVertsAreas,8);
		rearCol = Collisions::onSameSide(bodyRearVertsAreas,8);

		if(!bodyCol){
			cout<<"body collided"<<endl;
			//centre.z -= this->getForward().z * (dt * speed);
		}
		if(!wingRightCol){
			cout<<"right wing collided"<<endl;
			if(speed > 1500)
				rightWing = false;
			//centre -= this->getForward() * (dt * speed);
		}
		if(!wingLeftCol){
			cout<<"left wing collided"<<endl;
			if(speed > 1500)
				leftWing = false;
			//centre -= this->getForward() * (dt * speed);
		}
		if(!wingLeftCol || !wingRightCol || !propCol){
			if(!landed)
				centre += triNormal * (dt * speed);
			centre.y += (100*dt)/((speed+1)/10);
		}
		if(!wheelCol){
			cout<<"wheel collided"<<endl;
			centre.y -= this->getForward().y * (dt * speed);
			centre.y += (100*dt)/((speed+1)/10);
			speed -= dt*25;
			landed = true;
			
		}
		else{
			landed = false;
		}
		if(!propCol){
			cout<<"prop collided"<<endl;
			if(speed < 500)
				speed = 0;
			if(speed > 1500)
				respawn();
			//centre += triNormal * (dt * speed);
			//speed -= dt*50;
		}
		if(!rearCol){
			cout<<"rear collided"<<endl;
			if(speed > 1500)
				rear = false;
		}
		

	}
}

void Plane::collidePlanePart(Octree* oct, ThreeDModel* obj, glm::vec3* partVerts, int partType){
	
	vec3* octBB = oct->getBBVerts();
	
	vec3* octNormals = Collisions::calcBoxNormals(octBB);
	vec3* partNormals = Collisions::calcBoxNormals(partVerts);

	bool col = Collisions::OBBOBBCollision(partVerts, octBB, partNormals, octNormals);
	if(col){
		if(oct->getLevel() != MAX_DEPTH){
		//cout << "COLLISION at " << oct->getLevel() << endl;
			for(int i = 0; i < 8; i++){
				Octree* child = oct->getChild(i);
				if(child != NULL)
					collidePlanePart(child,obj, partVerts, partType);
			}
		}
		else{
			//cout << "COLLISION at leaf" << endl;
			//collidePolyPlanePart(oct, obj, partVerts, partType);
			Octree* ppOct = planeParts[partType]->octree;
			collidePlanePartOct(ppOct,obj, oct, octBB, partType);
		}
	}
}

void Plane::collidePlanePartOct(Octree* ppOct, ThreeDModel* worldObj, Octree* worldOct, glm::vec3* worldBBVerts, int partType){
	glm::mat4 trans = glm::translate(glm::mat4(1.0), centre);
	glm::mat4 rot = glm::mat4_cast(qorientation);
	glm::mat4 modelMat = trans * rot;
	vec3* octBB = ppOct->getBBVerts();
	for(int i = 0; i < 8; i++){
		octBB[i] = glm::vec3(modelMat * glm::vec4(octBB[i], 1.0));
	}
	vec3* octNormals = Collisions::calcBoxNormals(octBB);
	vec3* worldBBNormals = Collisions::calcBoxNormals(worldBBVerts);
	//cout << "in plane part octree" << endl;
	bool col = Collisions::OBBOBBCollision(octBB,worldBBVerts ,octNormals ,worldBBNormals );
	if(col){
		//cout << "collided at level " << ppOct->getLevel() << endl;
		if(ppOct->getLevel() != MAX_DEPTH){
		//cout << "COLLISION at " << oct->getLevel() << endl;
			for(int i = 0; i < 8; i++){
				Octree* child = ppOct->getChild(i);
				if(child != NULL)
					collidePlanePartOct(child,worldObj, worldOct, worldBBVerts, partType);
			}
		}
		else{
			//cout << "COLLISION at leaf" << endl;
			collidePolyPlanePart(worldOct, worldObj, octBB, partType);
		}
	}
}

void Plane::collidePolyPlanePart(Octree* oct, ThreeDModel* obj, glm::vec3* partVerts, int partType){
	int* prims = oct->getPrimitiveList();
	int primsLength = oct->getPrimitiveListSize();
	
	for(int i = 0; i < primsLength; i++){
		glm::vec3 triVerts[3];

		triVerts[0] = glm::vec3(obj->theVerts[obj->theFaces[prims[i]].thePoints[0]].x,
								obj->theVerts[obj->theFaces[prims[i]].thePoints[0]].y,
								obj->theVerts[obj->theFaces[prims[i]].thePoints[0]].z);

		triVerts[1] = glm::vec3(obj->theVerts[obj->theFaces[prims[i]].thePoints[1]].x, 
								obj->theVerts[obj->theFaces[prims[i]].thePoints[1]].y,
								obj->theVerts[obj->theFaces[prims[i]].thePoints[1]].z);

		triVerts[2] = glm::vec3(obj->theVerts[obj->theFaces[prims[i]].thePoints[2]].x,
								obj->theVerts[obj->theFaces[prims[i]].thePoints[2]].y,
								obj->theVerts[obj->theFaces[prims[i]].thePoints[2]].z);

		glm::vec3 triNormal = Collisions::getTriNormal(triVerts[0], triVerts[1], triVerts[2]);

		float partVertsAreas[8];
		for(int j = 0; j < 8; j++){		
			partVertsAreas[j] = Collisions::onLeftTri(triVerts[0], triVerts[1], triVerts[2], partVerts[j]);
		}

		//bool noCol = Collisions::OBBTriCollision(partVerts, triVerts, Collisions::calcBoxNormals(partVerts),triNormal);

		bool noCol = Collisions::onSameSide(partVertsAreas,8);
		if(!noCol){
			
			switch(partType){
				case 0:
					cout<<"body collided"<<endl;
					crashed = true;
					break;
				case 1:
					cout<<"right wing collided"<<endl;
					centre += triNormal * (dt * speed);
					if(speed > 500)
						rightWing = false;
					break;
				case 2:
					cout<<"left wing collided"<<endl;
					centre += triNormal * (dt * speed);
					if(speed > 500)
						leftWing = false;
					break;
				case 3:
					cout<<"wheel collided"<<endl;
					if(triNormal.y > 0.7)
						setUp(triNormal);
					//centre += triNormal * (dt * speed);
					//centre.y -= getForward().y * (dt * speed);
					centre += (glm::vec3(0,100,0) * dt);
					speed -= dt*25;
					landed = true;
					break;
				case 4:
					cout<<"prop collided"<<endl;
					centre += triNormal * (dt * speed);
					if(speed < 500)
						speed = 0;
					if(speed > 500)
						crashed = true;
					break;
				case 5:
					cout<<"rear collided"<<endl;
					centre += triNormal * (dt * speed);
					if(speed > 500)
						rear = false;
					break;

			}

		}
		if(partType == WHEELS && noCol){
			landed = false;
		}
		

	}

}

void Plane::setUp(glm::vec3 up){
	glm::mat4 rot = glm::mat4_cast(qorientation);
	rot[1][0] = up[0];
	rot[1][1] = up[1];
	rot[1][2] = up[2];
	qorientation = glm::quat_cast(rot);
}

