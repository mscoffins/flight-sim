#pragma once
#include "glm\glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "shaders\Shader.h"
#include "3dStruct\threeDModel.h"
#include "glm\gtc\quaternion.hpp"
#include "glm\gtx\rotate_vector.hpp"
#include "glm\gtc\type_ptr.hpp"
#include "Octree\Octree.h"
#include "BoundingSphere.h"
#include "Collisions.h"
#include "Bullet.h"
const float PI = 3.141; 
enum PartType{BODY_FRONT,WING_RIGHT,WING_LEFT,WHEELS,PROP,BODY_REAR};
class Plane{
private:
	vector<ThreeDModel*> planeParts;
	vector<Bullet*> bullets;
	vector<Bullet*> firedBullets;
	glm::vec3 centre;
	float speed;
	float dt;
	float fireDelay;
	
public:
	Plane();
	Plane(vector<ThreeDModel*> m, glm::vec3 c, ThreeDModel* bullet);
	void update(float deltaT);
	void draw(Shader *myShader);
	void drawProp(Shader *myShader);
	void setSpeed(float s){speed = s;}
	float getSpeed(){return speed;}
	float getYaw();
	float getRoll();
	float getPitch();
	vector<ThreeDModel*> getModel(){return planeParts;}
	vector<Bullet*> getFiredBullets(){return firedBullets;}
	void fire();
	void reload(ThreeDModel* b);
	glm::vec3 getCentre(){return centre;}
	glm::vec3 getForward();
	glm::vec3 getUp();
	glm::vec3 getRight();
	void setUp(glm::vec3 up);
	void resetQuats();
	void respawn();
	void collisionDetection(vector<ThreeDModel*> world);
	void collideOctNode(Octree* oct, ThreeDModel* obj);
	void collidePoly(Octree* oct, ThreeDModel* obj);

	void collidePlanePart(Octree* oct, ThreeDModel* obj, glm::vec3* partVerts, int partType);
	void collidePlanePartOct(Octree* ppOct, ThreeDModel* worldObj, Octree* worldOct, glm::vec3* worldBBVerts, int partType);
	void collidePolyPlanePart(Octree* oct, ThreeDModel* obj, glm::vec3* partVerts, int partType);

	glm::quat  qorientation,qroll, qpitch, qyaw, propQOrientation;
	bool crashed;
	bool landed;
	bool leftWing, rightWing, rear;
	float propRot;
};