#include "glm\glm.hpp"
#include "Sphere.h"
class BoundingSphere{
private:
	float rad;
	glm::vec3 centre;
public:
	BoundingSphere(float r = 1, glm::vec3 c = glm::vec3(0.0f,0.0f,0.0f)){
		rad = r;
		centre = c;
	}

	float getRad(){return rad;}
	glm::vec3 getCentre(){return centre;}
	void setCentre(glm::vec3 c){centre = c;}
	void setRad(float r){rad = r;}
};
